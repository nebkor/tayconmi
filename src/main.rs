use crossbeam;
use nannou::image::{self, jpeg::JpegDecoder};
use nannou::prelude::*;
use rscam;

use std::thread;

const HEIGHT: u32 = 720;
const WIDTH: u32 = 1280;
const FPS: u32 = 30;

// The vertex type that we will use to represent a point on our triangle.
#[repr(C)]
#[derive(Clone, Copy)]
struct Vertex {
    position: [f32; 2],
}

// The vertices that make up the rectangle to which the image will be drawn.
const VERTICES: [Vertex; 4] = [
    Vertex {
        position: [-1.0, -1.0],
    },
    Vertex {
        position: [-1.0, 1.0],
    },
    Vertex {
        position: [1.0, -1.0],
    },
    Vertex {
        position: [1.0, 1.0],
    },
];

impl wgpu::VertexDescriptor for Vertex {
    const STRIDE: wgpu::BufferAddress = std::mem::size_of::<Vertex>() as _;
    const ATTRIBUTES: &'static [wgpu::VertexAttributeDescriptor] =
        &[wgpu::VertexAttributeDescriptor {
            format: wgpu::VertexFormat::Float2,
            offset: 0,
            shader_location: 0,
        }];
}

fn main() {
    nannou::app(mk_model).update(update_model).run();
}

struct Model {
    cam_feed: crossbeam::channel::Receiver<image::DynamicImage>,
    texture: wgpu::Texture,
    texture_view: wgpu::TextureView,
    sampler: wgpu::Sampler,
    bind_group_layout: wgpu::BindGroupLayout,
    bind_group: wgpu::BindGroup,
    render_pipeline: wgpu::RenderPipeline,
    vertex_buffer: wgpu::Buffer,
}

fn mk_model(app: &App) -> Model {
    app.set_loop_mode(LoopMode::RefreshSync);

    let w_id = app
        .new_window()
        .size(WIDTH, HEIGHT)
        .view(view)
        .build()
        .unwrap();
    let window = app.window(w_id).unwrap();
    let device = window.swap_chain_device();
    let format = Frame::TEXTURE_FORMAT;
    let msaa_samples = window.msaa_samples();

    let vs_mod = wgpu::shader_from_spirv_bytes(device, include_bytes!("../shaders/vert.spv"));
    let fs_mod = wgpu::shader_from_spirv_bytes(device, include_bytes!("../shaders/frag.spv"));

    let (camw, camr) = crossbeam::channel::bounded(0);
    let mut camera = rscam::new("/dev/video0").unwrap();
    camera
        .start(&rscam::Config {
            interval: (1, FPS),
            resolution: (WIDTH, HEIGHT),
            format: b"MJPG",
            ..Default::default()
        })
        .unwrap();
    // move our camera into a separate thread
    thread::spawn(move || {
        let mut count = 0;
        let mut ts: u64 = 0;
        loop {
            let frame = camera.capture().unwrap();
            let jpg = JpegDecoder::new(frame.as_ref()).unwrap();
            if let Ok(img) = image::DynamicImage::from_decoder(jpg) {
                let _ = camw.send(img).ok();
            }
            if count > (FPS - 2) {
                let cur_ts = frame.get_timestamp();
                let delta = cur_ts - ts;
                let fps = (FPS as u64 * 1_000_000) / delta;
                println!("fps: {}", fps);
                count = 0;
                ts = cur_ts;
            } else {
                count += 1;
            }
        }
    });

    let img: image::RgbaImage = image::ImageBuffer::new(WIDTH, HEIGHT);

    let texture = wgpu::Texture::from_image(app, &image::DynamicImage::ImageRgba8(img));
    let texture_view = texture.view().build();

    // Create the sampler for sampling from the source texture.
    let sampler = wgpu::SamplerBuilder::new().build(device);

    let bind_group_layout = create_bind_group_layout(device);
    let bind_group = create_bind_group(device, &bind_group_layout, &texture_view, &sampler);
    let pipeline_layout = create_pipeline_layout(device, &bind_group_layout);
    let render_pipeline = create_render_pipeline(
        device,
        &pipeline_layout,
        &vs_mod,
        &fs_mod,
        format,
        msaa_samples,
    );

    // Create the vertex buffer.
    let vertex_buffer = device
        .create_buffer_mapped(VERTICES.len(), wgpu::BufferUsage::VERTEX)
        .fill_from_slice(&VERTICES[..]);

    Model {
        cam_feed: camr,
        texture,
        texture_view,
        sampler,
        bind_group_layout,
        bind_group,
        render_pipeline,
        vertex_buffer,
    }
}

fn update_model(app: &App, model: &mut Model, _update: Update) {
    let window = app.main_window();
    let device = window.swap_chain_device();

    if let Ok(img) = model.cam_feed.recv() {
        // a new one on each frame.
        model.texture = wgpu::Texture::from_image(app, &img);
        model.texture_view = model.texture.view().build();
        model.bind_group = create_bind_group(
            device,
            &model.bind_group_layout,
            &model.texture_view,
            &model.sampler,
        );
    }
}

fn view(_app: &App, model: &Model, frame: Frame) {
    let mut encoder = frame.command_encoder();
    let mut render_pass = wgpu::RenderPassBuilder::new()
        .color_attachment(frame.texture_view(), |color| color)
        .begin(&mut encoder);
    render_pass.set_bind_group(0, &model.bind_group, &[]);
    render_pass.set_pipeline(&model.render_pipeline);
    render_pass.set_vertex_buffers(0, &[(&model.vertex_buffer, 0)]);
    let vertex_range = 0..VERTICES.len() as u32;
    let instance_range = 0..1;
    render_pass.draw(vertex_range, instance_range);
}

fn create_bind_group_layout(device: &wgpu::Device) -> wgpu::BindGroupLayout {
    wgpu::BindGroupLayoutBuilder::new()
        .sampled_texture(
            wgpu::ShaderStage::FRAGMENT,
            false,
            wgpu::TextureViewDimension::D2,
        )
        .sampler(wgpu::ShaderStage::FRAGMENT)
        .build(device)
}

fn create_bind_group(
    device: &wgpu::Device,
    layout: &wgpu::BindGroupLayout,
    texture: &wgpu::TextureView,
    sampler: &wgpu::Sampler,
) -> wgpu::BindGroup {
    wgpu::BindGroupBuilder::new()
        .texture_view(texture)
        .sampler(sampler)
        .build(device, layout)
}

fn create_pipeline_layout(
    device: &wgpu::Device,
    bind_group_layout: &wgpu::BindGroupLayout,
) -> wgpu::PipelineLayout {
    let desc = wgpu::PipelineLayoutDescriptor {
        bind_group_layouts: &[&bind_group_layout],
    };
    device.create_pipeline_layout(&desc)
}

fn create_render_pipeline(
    device: &wgpu::Device,
    layout: &wgpu::PipelineLayout,
    vs_mod: &wgpu::ShaderModule,
    fs_mod: &wgpu::ShaderModule,
    dst_format: wgpu::TextureFormat,
    sample_count: u32,
) -> wgpu::RenderPipeline {
    wgpu::RenderPipelineBuilder::from_layout(layout, vs_mod)
        .fragment_shader(fs_mod)
        .color_format(dst_format)
        .add_vertex_buffer::<Vertex>()
        .sample_count(sample_count)
        .primitive_topology(wgpu::PrimitiveTopology::TriangleStrip)
        .build(device)
}
